import java.util.*;

public class Game {
	static ArrayList<Integer> playerX = new ArrayList<>();
	static ArrayList<Integer> playerO = new ArrayList<>();
	static int status = 0;
	static int changePos = 0;

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		int turn = 0;
		char player = ' ';
		char[][] board = { { '_', '_', '_' }, { '_', '_', '_' }, { '_', '_', '_' } };

		System.out.println("Welcome to OX Game");
		printBoard(board);
		System.out.println();
		while (true) {

			if (turn % 2 == 0) {
				player = 'X';
			} else {
				player = 'O';
			}
			System.out.println("Turn " + player);
			System.out.print("Please input Row Col: \n");

			int row = kb.nextInt();
			int col = kb.nextInt();
			// ------------------------------//
			int pos = position(row, col, player);
			while (playerX.contains(pos) || playerO.contains(pos)) {
				System.out.println("ERROR! : New input Row Col : ");
				row = kb.nextInt();
				col = kb.nextInt();
				pos = position(row, col, player);
			}

			writePosition(board, row, col, player);
			printBoard(board);
			System.out.println();
			if (turn == 8) {
				System.out.println("--- Draw ---");
				break;
			} else {
				checkWinner();
				if (status == 1) {
					System.out.println("Player " + player + " win\nBye Bye");
					break;
				} else
					turn++;
			}
		}
	}

	public static void printBoard(char[][] Board) {
		int numberRow = 1;
		System.out.println(" 1 2 3");
		for (char[] row : Board) {
			System.out.print(numberRow);
			for (char col : row) {
				System.out.print(col + " ");
			}
			numberRow++;
			System.out.println();
		}
	}

	public static void writePosition(char[][] board, int row, int col, char user) {
		if (user == 'X') {
			board[row-1][col-1] = 'X';
			position(row, col, user);
			playerX.add(changePos);
		} else if (user == 'O') {
			board[row-1][col-1] = 'O';
			position(row, col, user);
			playerO.add(changePos);
		}
	}

	public static int position(int row, int col, char user) {
		switch (user) {
		case 'X':
			switch (row) {
			case 1:
				if (col == 1) {
					return changePos = 1;

				} else if (col == 2) {
					return changePos = 2;

				} else if (col == 3) {
					return changePos = 3;
				}
				break;

			case 2:
				if (col == 1) {
					return changePos = 4;

				} else if (col == 2) {
					return changePos = 5;

				} else if (col == 3) {
					return changePos = 6;
				}
				break;

			case 3:
				if (col == 1) {
					return changePos = 7;

				} else if (col == 2) {
					return changePos = 8;

				} else if (col == 3) {
					return changePos = 9;
				}

				break;
			default:
				break;
			}
			break;

		case 'O':
			switch (row) {
			case 1:
				if (col == 1) {
					return changePos = 1;

				} else if (col == 2) {
					return changePos = 2;

				} else if (col == 3) {
					return changePos = 3;
				}
				break;

			case 2:
				if (col == 1) {
					return changePos = 4;

				} else if (col == 2) {
					return changePos = 5;

				} else if (col == 3) {
					return changePos = 6;
				}
				break;

			case 3:
				if (col == 1) {
					return changePos = 7;

				} else if (col == 2) {
					return changePos = 8;

				} else if (col == 3) {
					return changePos = 9;
				}

				break;
			default:
				break;
			}
			break;
		}
		return 0;
	}

	public static void checkWinner() {

		List<Integer> top = new ArrayList<>();
		top.add(1);
		top.add(2);
		top.add(3);
		List<Integer> mid = new ArrayList<>();
		mid.add(4);
		mid.add(5);
		mid.add(6);
		List<Integer> bot = new ArrayList<>();
		bot.add(7);
		bot.add(8);
		bot.add(9);

		List<Integer> col1 = new ArrayList<>();
		col1.add(1);
		col1.add(4);
		col1.add(7);
		List<Integer> col2 = new ArrayList<>();
		col2.add(2);
		col2.add(5);
		col2.add(8);
		List<Integer> col3 = new ArrayList<>();
		col3.add(3);
		col3.add(6);
		col3.add(9);

		List<Integer> cross1 = new ArrayList<>();
		cross1.add(1);
		cross1.add(5);
		cross1.add(9);
		List<Integer> cross2 = new ArrayList<>();
		cross2.add(3);
		cross2.add(5);
		cross2.add(7);

		ArrayList<List> winner = new ArrayList<>();
		winner.add(top);
		winner.add(mid);
		winner.add(bot);
		winner.add(col1);
		winner.add(col2);
		winner.add(col3);
		winner.add(cross1);
		winner.add(cross2);

		for (List win : winner) {
			if (playerX.containsAll(win)) {
				status++;
			} else if (playerO.containsAll(win)) {
				status++;
			}
		}
	}

}